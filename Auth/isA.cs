﻿using healthyGym.Enums;
using healthyGym.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace healthyGym.Auth
{
    public class isA
    {
        public static bool SuperAdmin()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if (type == (int)UserTypes.SuperAdmin)
                return true;
            return false;
        }

        public static bool Owner()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if (type == (int)UserTypes.Owner)
                return true;
            return false;
        }

        public static bool Admin()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if (type == (int)UserTypes.Admin)
                return true;
            return false;
        }

        public static bool Staff()
        {
            int type = System.Web.HttpContext.Current.Session["type"].ToString().ToInt();
            if (type == (int)UserTypes.Staff)
                return true;
            return false;
        }

    }
}