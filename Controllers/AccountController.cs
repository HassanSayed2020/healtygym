﻿using healthyGym.Models;
using healthyGym.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace healthyGym.Controllers
{
    public class AccountController : Controller
    {
        DBContext db = new DBContext();

        // GET: Account
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(User user)
        {
         
            if (db.users.Any(s => s.user_name == user.user_name && s.password == user.password))
            {
                User currentUser = db.users.Where(s => s.user_name == user.user_name && s.password == user.password).FirstOrDefault();
                if(currentUser.active != 1)
                    ViewBag.errorMsg = "This User isnot Active.";
                else
                {
                    Session["id"] = currentUser.id;
                    Session["code"] = currentUser.code;
                    Session["user_name"] = currentUser.user_name;
                    Session["email"] = currentUser.email;
                    Session["gender"] = currentUser.gender;
                    Session["gym_id"] = currentUser.gym_id;
                    Session["gym_name"] = null;
                    if (currentUser.gym_id > 0)
                    { 
                        var gym = db.gyms.Find(currentUser.gym_id);
                        if (gym != null)
                        {
                            Session["gym_name"] = gym.name;
                        }

                    }
                    Session["branch_id"] = currentUser.branch_id != null ? currentUser.branch_id : 0;
                    Session["branch_name"] = null;
                    if (currentUser.branch_id > 0)
                    {
                        var branch = db.branches.Find(currentUser.branch_id);
                        if (branch != null)
                        {
                            Session["branch_name"] = branch.name;
                        }

                    }
                    Session["type"] = currentUser.type;
                    Session["order"] = new List<OrderViewModel>();
                    return RedirectToAction("Index", "Dashboard");
                }
                
            }
            else
                ViewBag.errorMsg = "Invalid Username Or Password";
            return View();
        }
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Login");
        }
    }
}