﻿using healthyGym.Auth;
using healthyGym.Helpers;
using healthyGym.Models;
using healthyGym.ViewModel;
using healthyGym.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace healthyGym.Controllers
{
    [CustomAuthenticationFilter]
    public class CalenderController : Controller
    {
        DBContext db = new DBContext();
        // GET: Calender
        public ActionResult Index()
        {
            ViewBag.Packages = db.packages.Where(p => p.coach_price != 0).Select(s => new { s.id, s.name }).ToList();
            ViewBag.Users = db.users.Where(u=>u.type == (int)UserTypes.Staff).Select(s => new { s.id, s.full_name }).ToList();
            ViewBag.Rooms = db.rooms.Select(s => new { s.id, s.Description }).ToList();

            return View();

        }

        public JsonResult SaveSchedule(ScheduleViewModel scheduleViewModel)
        {
            if(scheduleViewModel.id == 0)
            {
                Schedule schedule = AutoMapper.Mapper.Map<ScheduleViewModel, Schedule>(scheduleViewModel);
                schedule.updated_at = DateTime.Now;
                schedule.created_by = Session["id"].ToString().ToInt();
                schedule.created_at = DateTime.Now;
                schedule.branch_id = (int)Session["branch_id"];
                db.schedules.Add(schedule);
                db.SaveChanges();
            }
            return Json(new { msg = "done" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSchedule()
        {
            List<CalenderViewModel> calender = db.schedules.Select(s => new CalenderViewModel
            {
                id = s.id,
                title = s.title,
                start = s.start.ToString(),
                end = s.end.ToString(),
                className = s.className,
                description = s.description

            }).AsEnumerable()                                                                 
            .Select(s => new CalenderViewModel
            {
                id = s.id,
                title = s.title,
                start = DateTime.SpecifyKind(Convert.ToDateTime(s.start), DateTimeKind.Utc).ToString("s"),
                end = DateTime.SpecifyKind(Convert.ToDateTime(s.end), DateTimeKind.Utc).ToString("s"),
                className = s.className,
                description = s.description
            }).ToList();

            return Json(new { calender = calender }, JsonRequestBehavior.AllowGet);
        }
    }
}