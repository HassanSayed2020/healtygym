﻿using healthyGym.Auth;
using healthyGym.Helpers;
using healthyGym.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace healthyGym.Controllers
{
    [CustomAuthenticationFilter]
    public class DashboardController : Controller
    {

        DBContext db = new DBContext();
        // GET: Dashboard
        public ActionResult Index()
        {
            //int branchId = Session["gym_id"].ToString().ToInt();
            //ViewBag.Branches = db.branches.Where(b => b.gym_id == branchId).ToList();
            return View();
        }

        public ActionResult changeBranch(int branchId)
        {
            Session["branch_id"] = branchId;
            Session["branch_name"] = null;
            if (branchId > 0)
            {
                var branch = db.branches.Find(branchId);
                if (branch != null)
                {
                    Session["branch_name"] = branch.name;
                }

            }
            return RedirectToAction("Index");
        }
    }
}