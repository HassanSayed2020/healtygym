﻿using healthyGym.Auth;
using healthyGym.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using healthyGym.ViewModel;
using System.Globalization;
using healthyGym.Helpers;
using healthyGym.Enums;
using System.IO;

namespace healthyGym.Controllers
{
    [CustomAuthenticationFilter]
    public class GymController : Controller
    {
        DBContext db = new DBContext();
        // GET: Gym


        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                //var draw = Request.Form.GetValues("draw")[0];
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                //var sortColumn = Request.QueryString["columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]"];
                //var sortColumnDir = Request.QueryString["order[0][dir]"];
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var from_date = Request.Form.GetValues("columns[0][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;


                //var data = db.gyms.ToList();

                // Getting all data    
                var gymData = (from gym in db.gyms
                               join u in db.users on gym.created_by equals u.id
                               join ow in db.users on gym.id equals ow.gym_id into gyow
                               from gow in gyow.DefaultIfEmpty()
                               select new GymViewModel
                               {
                                   id = gym.id,
                                   code = gym.code,
                                   name = gym.name,
                                   description = gym.description,
                                   is_active = gym.is_active,
                                   created_by = u.user_name,
                                   created_at = gym.created_at.ToString(),
                                   updated_at = gym.updated_at.ToString(),
                                   branches = (from gy in db.gyms
                                               join b in db.branches on gy.id equals b.gym_id into gyb
                                               from gb in gyb.DefaultIfEmpty()
                                               select new BranchViewModel
                                               {
                                                   id = gy.id,
                                                   name = (gb == null ? String.Empty : gb.name),

                                               }).Where(p => p.id == gym.id).ToList(),
                                   user_name = (gow == null ? String.Empty : gow.user_name),
                                   password = (gow == null ? String.Empty : gow.password),
                                   address1 = (gow == null ? String.Empty : gow.address1),
                                   phone1 = (gow == null ? String.Empty : gow.phone1),
                                   email = (gow == null ? String.Empty : gow.email),
                                   imagePath = (gow == null ? String.Empty : gow.image),
                                   full_name = (gow == null ? String.Empty : gow.full_name),
                                   type = (gow == null ? 0 : gow.type),
                               }).Where(t => t.type == (int)UserTypes.Owner);
                // db.gyms.OrderBy(s => s.id);
                
                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    gymData = gymData.Where(m => m.name.ToLower().Contains(searchValue.ToLower()) || m.code.ToLower().Contains(searchValue.ToLower()) ||
                     m.description.ToLower().Contains(searchValue.ToLower()) || m.created_by.ToLower().Contains(searchValue.ToLower()));
                }

                //if (!string.IsNullOrEmpty(from_date))
                //{
                //    //CultureInfo provider = CultureInfo.InvariantCulture;
                //    //DateTime dateTime10 = DateTime.ParseExact(from_date, "yyyy/mm/dd", provider);
                //    gymData = gymData.Where(m => Convert.ToDateTime(m.created_at) >= Convert.ToDateTime(from_date));
                //}

                //if (!string.IsNullOrEmpty(to_date))
                //{

                //        //CultureInfo provider = CultureInfo.InvariantCulture;
                //        //DateTime dateTime10 = DateTime.ParseExact(to_date, "yyyy/mm/dd", provider);
                //        gymData = gymData.Where(m => Convert.ToDateTime(m.created_at) <= Convert.ToDateTime(to_date));
                //}
                //total number of rows count     

                var displayResult = gymData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = gymData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }
            return View();
        }
        [HttpPost]
        public JsonResult saveGym(GymViewModel gymVM)
        {

            if (gymVM.id == 0)
            {
                Gym gym = AutoMapper.Mapper.Map<GymViewModel, Gym>(gymVM);
                gym.updated_at = DateTime.Now;
                gym.created_by = Session["id"].ToString().ToInt();
                gym.created_at = DateTime.Now;
                db.gyms.Add(gym);
                db.SaveChanges();

                User user = AutoMapper.Mapper.Map<GymViewModel, User>(gymVM);
                if (gymVM.image != null)
                {
                    Guid guid = Guid.NewGuid();
                    var InputFileName = Path.GetFileName(gymVM.image.FileName);
                    var ServerSavePath = Path.Combine(Server.MapPath("~/images/ownerProfile/") + guid.ToString() + "_Profile" + Path.GetExtension(gymVM.image.FileName));
                    gymVM.image.SaveAs(ServerSavePath);
                    user.image = "/images/ownerProfile/" + guid.ToString() + "_Profile" + Path.GetExtension(gymVM.image.FileName);
                }

                user.gym_id = gym.id;
                user.type = (int)UserTypes.Owner;
                user.created_at = DateTime.Now;
                user.updated_at = DateTime.Now;
                user.created_by = Session["id"].ToString().ToInt();
                db.users.Add(user);
                db.SaveChanges();

                Staff staff = AutoMapper.Mapper.Map<GymViewModel, Staff>(gymVM);
                staff.staff_id = user.id;
                db.staffs.Add(staff);
            }
            else
            {
                Gym oldGym = db.gyms.Find(gymVM.id);
                oldGym.code = gymVM.code;
                oldGym.name = gymVM.name;
                oldGym.description = gymVM.description;
                oldGym.is_active = gymVM.is_active;
                db.Entry(oldGym).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                User oldUser = db.users.Where(u => u.gym_id == oldGym.id && u.type == (int)UserTypes.Owner).FirstOrDefault();

                if (gymVM.image != null)
                {
                    Guid guid = Guid.NewGuid();
                    var InputFileName = Path.GetFileName(gymVM.image.FileName);
                    var ServerSavePath = Path.Combine(Server.MapPath("~/images/ownerProfile/") + guid.ToString() + "_Profile" + Path.GetExtension(gymVM.image.FileName));
                    gymVM.image.SaveAs(ServerSavePath);
                    oldUser.image = "/images/ownerProfile/" + guid.ToString() + "_Profile" + Path.GetExtension(gymVM.image.FileName);
                }

                oldUser.user_name = gymVM.user_name;
                oldUser.password = gymVM.password;
                oldUser.phone1 = gymVM.phone1;
                oldUser.full_name = gymVM.full_name;
                oldUser.email = oldUser.email;
                //oldUser.gender = staffVM.gender;
                oldUser.address1 = gymVM.address1;
                oldUser.updated_at = DateTime.Now;

                db.Entry(oldUser).State = System.Data.Entity.EntityState.Modified;
            }
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult deleteGym(int id)
        {
            Gym deletedGym = db.gyms.Find(id);

            User deletedUser = db.users.Where(u => u.gym_id == deletedGym.id && u.type == (int)UserTypes.Owner).FirstOrDefault();

            Staff deletedStaff = db.staffs.Find(deletedUser.id);
            db.staffs.Remove(deletedStaff);
            db.SaveChanges();

            db.users.Remove(deletedUser);
            db.SaveChanges();

            db.gyms.Remove(deletedGym);
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

    }
}


