﻿using healthyGym.Auth;
using healthyGym.Helpers;
using healthyGym.Models;
using healthyGym.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace healthyGym.Controllers
{
    [CustomAuthenticationFilter]
    public class InviteController : Controller
    {
        DBContext db = new DBContext();
        // GET: Invite
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var from_date = Request.Form.GetValues("columns[0][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data
                int branchID = (int)Session["branch_id"];
                var inviteData = (from subscribtion in db.subscribtions
                                    join member in db.members on subscribtion.member_id equals member.member_id
                                    join user in db.users on subscribtion.member_id equals user.id
                                    join invite in db.invites on subscribtion.id equals invite.subscribtion_id
                                    join creation in db.users on invite.created_by equals creation.id
                                    select new InviteViewModel
                                    {
                                        id = invite.id,
                                        code = user.code,
                                        member_name = user.full_name,
                                        member_id = user.id,
                                        invitee_name = invite.name,
                                        created_at = invite.created_at,
                                        phone = invite.phone,
                                        address = invite.address,
                                        created_by = creation.full_name,
                                        branch_id = user.branch_id
                                    }).Where(g => g.branch_id == branchID);

                //Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    inviteData = inviteData.Where(m => m.code.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.member_name.ToLower().Contains(searchValue.ToLower()) || m.invitee_name.ToLower().Contains(searchValue.ToLower()) || m.created_by.ToLower().Contains(searchValue.ToLower())
                     || m.address.ToLower().Contains(searchValue.ToLower()));
                }

                var displayResult = inviteData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = inviteData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }
            ViewBag.members = (from user in db.users
                           join member in db.members on user.id equals member.member_id
                           select new
                           {
                            user.id,
                            user.full_name
                           }).ToList();
            
            return View();
        }

        [HttpPost]
        public JsonResult saveInvite(InviteViewModel inviteVM)
        {

            if ((int)Session["branch_id"] > 0)
            {
                if (inviteVM.id == 0)
                {
                    Invite invite = AutoMapper.Mapper.Map<InviteViewModel, Invite>(inviteVM);

                    invite.updated_at = DateTime.Now;
                    invite.created_by = Session["id"].ToString().ToInt();
                    invite.created_at = DateTime.Now;
                    invite.name = inviteVM.invitee_name;

                    if (canInvite(inviteVM.member_id) == 0)
                        return Json(new { status = false, message = "Sorry! Invitation Limit Excided." }, JsonRequestBehavior.AllowGet);
                    
                    invite.subscribtion_id = canInvite(inviteVM.member_id);

                    db.invites.Add(invite);
                }
                else
                {
                    Invite oldInvite = db.invites.Find(inviteVM.id);

                    oldInvite.phone = inviteVM.phone;
                    oldInvite.address = inviteVM.address;
                    oldInvite.note = inviteVM.note;
                    oldInvite.updated_at = DateTime.Now;
                    oldInvite.created_by = Session["id"].ToString().ToInt();
                    oldInvite.created_at = DateTime.Now;
                    oldInvite.name = inviteVM.invitee_name;

                    if (canInvite(inviteVM.member_id) == 0)
                        return Json(new { status = false, message = "Invitation Limit Excided" }, JsonRequestBehavior.AllowGet);

                    oldInvite.subscribtion_id = canInvite(inviteVM.member_id);

                    db.Entry(oldInvite).State = System.Data.Entity.EntityState.Modified;
                }
                db.SaveChanges();

                return Json(new { status = true, message = "done" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, message = "Branch is not Selected" }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult deleteInvite(int id)
        {
            Invite deleteInvite = db.invites.Find(id);
            db.invites.Remove(deleteInvite);
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public int canInvite(int id)
        {

            Subscribtion subscribtion = db.subscribtions.Where(s => s.due_date >= DateTime.Now && s.member_id == id && s.is_active == true).FirstOrDefault();
            Package package = db.packages.Where(p => p.id == subscribtion.package_id).FirstOrDefault();
            int currentInvites = db.invites.Where(i => i.subscribtion_id == subscribtion.id).Count();
            if (currentInvites < package.no_of_invites)
                return subscribtion.id;
            else
                return 0;
    
        }
    }
}