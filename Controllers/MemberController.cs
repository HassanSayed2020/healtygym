﻿using healthyGym.Auth;
using healthyGym.Helpers;
using healthyGym.Models;
using healthyGym.ViewModel;
using healthyGym.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace healthyGym.Controllers
{
    [CustomAuthenticationFilter]
    public class MemberController : Controller
    {
        DBContext db = new DBContext();
        // GET: Member
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var subFilter = Request.Form.GetValues("columns[0][search][value]")[0];
                //var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data    
                int branchID = (int)Session["branch_id"];
                var memberData = (from user in db.users
                                 join member in db.members on user.id equals member.member_id
                                 join branch in db.branches on user.branch_id equals branch.id
                                 join gym in db.gyms on user.gym_id equals gym.id
                                 join creation in db.users on user.created_by equals creation.id
                                 select new MemberViewModel
                                 {
                                     id = user.id,
                                     full_name = user.full_name,
                                     code = user.code,
                                     user_name = user.user_name,
                                     email = user.email,
                                     phone1 = user.phone1,
                                     phone2 = user.phone2,
                                     address1 = user.address1,
                                     address2 = user.address2,
                                     gender = user.gender,
                                     birthDate = user.birthDate,
                                     nationality = user.nationality,
                                     gym_id = user.gym_id,
                                     branch_id = user.branch_id,
                                     active = user.active,
                                     imagePath = user.image,
                                     gym_name = gym.name,
                                     branch_name = branch.name,
                                     created_by = creation.user_name,
                                     subscribtion_id = member.subscribtion_id,
                                     subscribtionViewModel = (from subscribtion in db.subscribtions
                                                             join member in db.members on subscribtion.member_id equals member.member_id
                                                             join user in db.users on subscribtion.member_id equals user.id
                                                             join branch in db.branches on user.branch_id equals branch.id
                                                             join gym in db.gyms on user.gym_id equals gym.id
                                                             join subscriptionCreation in db.users on subscribtion.created_by equals subscriptionCreation.id
                                                             join package in db.packages on subscribtion.package_id equals package.id
                                                             join offer in db.offers on subscribtion.offer_id equals offer.id into ofSb
                                                             join coach in db.users on subscribtion.staff_id equals coach.id into coSb
                                                             from oSb in ofSb.DefaultIfEmpty()
                                                             from cSb in coSb.DefaultIfEmpty()
                                                             select new SubscribtionViewModel
                                                             {
                                                                 id = user.id,
                                                                 code = user.code,
                                                                 full_name = user.full_name,
                                                                 email = user.email,
                                                                 phone1 = user.phone1,
                                                                 phone2 = user.phone2,
                                                                 address1 = user.address1,
                                                                 address2 = user.address2,
                                                                 gender = user.gender,
                                                                 birthDate = user.birthDate,
                                                                 nationality = user.nationality,
                                                                 gym_id = user.gym_id,
                                                                 active = user.active,
                                                                 branch_id = user.branch_id,
                                                                 gym_name = gym.name,
                                                                 branch_name = branch.name,
                                                                 created_by = creation.user_name,
                                                                 subscribtion_datetime = subscribtion.subscribtion_datetime,
                                                                 cost = subscribtion.cost,
                                                                 note = subscribtion.note,
                                                                 start_date = subscribtion.start_date,
                                                                 due_date = subscribtion.due_date,
                                                                 start_date_string = subscribtion.start_date.ToString(),
                                                                 due_date_string = subscribtion.due_date.ToString(),
                                                                 is_active = subscribtion.is_active,
                                                                 member_id = subscribtion.member_id,
                                                                 package_id = subscribtion.package_id,
                                                                 offer_id = subscribtion.offer_id,
                                                                 staff_id = subscribtion.staff_id,
                                                                 subscribtion_id = subscribtion.id,
                                                                 package_name = package.name,
                                                                 package_month = package.month,
                                                                 package_price = package.price,
                                                                 offer_name = (oSb == null ? String.Empty : oSb.name),
                                                                 offer_amount = (oSb == null ? 0 : oSb.discount_amount),
                                                                 offer_type = (oSb == null ? 0 : oSb.offer_type),
                                                                 coach_name = (cSb == null ? String.Empty : cSb.full_name)

                                                             }).Where(s => s.subscribtion_id == member.subscribtion_id).FirstOrDefault()

                                     }).Where(b => b.branch_id == branchID);
                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    memberData = memberData.Where(m => m.full_name.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.email.ToLower().Contains(searchValue.ToLower()) || m.phone1.ToLower().Contains(searchValue.ToLower()) || m.code.ToLower().Contains(searchValue.ToLower())
                     || m.user_name.ToLower().Contains(searchValue.ToLower()) || m.address1.ToLower().Contains(searchValue.ToLower()) || m.address2.ToLower().Contains(searchValue.ToLower()));
                }
                //if (!string.IsNullOrEmpty(subFilter))
                //{
                //    if (Convert.ToInt32(subFilter) == 2)
                //    {
                //        memberData = memberData.Where(m => m.subscribtionViewModel.due_date < DateTime.Now);
                //    }
                //    else
                //    {
                //        memberData = memberData.Where(m => m.subscribtionViewModel.due_date >= DateTime.Now);
                //    }
                //}
                //else
                //{
                //    memberData = memberData.Where(m => m.subscribtionViewModel.due_date >= DateTime.Now);
                //}
                var displayResult = memberData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = memberData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }

            ViewBag.packages = db.packages.Select(s => new { s.id, s.name }).ToList();
            ViewBag.offers = db.offers.Select(s => new { s.id, s.name }).ToList();
            ViewBag.branches = db.branches.Select(s => new { s.id, s.name }).ToList();
            ViewBag.gyms = db.gyms.Select(s => new { s.id, s.name }).ToList();
            ViewBag.coaches = (from staff in db.staffs
                               join user in db.users  on staff.staff_id equals user.id
                               join staffRole in db.staffRoles on staff.staff_role_id equals staffRole.id
                               select new CoachViewModel
                               {
                                    id = staff.staff_id,
                                    full_name = user.full_name,
                                    role = staffRole.role
                               }).Where(s => s.role == "Coach").ToList();
            

            return View();
        }
        [HttpPost]
        public JsonResult renewSubscription(MemberViewModel memberVM)
        {
            Member member = db.members.Find(memberVM.id);
            if (member != null) { 
                Subscribtion sub = new Subscribtion();

                sub.cost = memberVM.package_price;
                sub.member_id = member.member_id;
                sub.start_date = memberVM.start_date;
                sub.due_date = memberVM.due_date;
                sub.note = memberVM.note;
                sub.offer_id = memberVM.offer_id;
                sub.package_id = memberVM.package_id;
                sub.subscribtion_datetime = DateTime.Now;
                sub.created_at = DateTime.Now;
                sub.updated_at = DateTime.Now;
                sub.created_by = Session["id"].ToString().ToInt();
                db.subscribtions.Add(sub);
                db.SaveChanges();

                member.subscribtion_id = sub.id;
                db.Entry(member).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return Json(new { message = "code is not valid" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult saveMember(MemberViewModel memberVM)
        {
            var checkAvailabilty = db.users.Any(s => s.code == memberVM.code);
            User user1 = db.users.Find(memberVM.id);
            if (!checkAvailabilty || (memberVM.id > 0 && user1.code == memberVM.code))
            {
                if ((int)Session["gym_id"] > 0 && (int)Session["branch_id"] > 0)
                {
                    if (memberVM.id == 0)
                    {
                        User user = AutoMapper.Mapper.Map<MemberViewModel, User>(memberVM);
                       
                        user.created_at = DateTime.Now;
                        user.updated_at = DateTime.Now;
                        user.created_by = Session["id"].ToString().ToInt();
                        user.type = (int)UserTypes.Staff;
                        if (memberVM.image != null)
                        {
                            Guid guid = Guid.NewGuid();
                            var InputFileName = Path.GetFileName(memberVM.image.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/images/memberProfile/") + guid.ToString() + "_Profile" + Path.GetExtension(memberVM.image.FileName));
                            memberVM.image.SaveAs(ServerSavePath);
                            user.image = "/images/memberProfile/" + guid.ToString() + "_Profile" + Path.GetExtension(memberVM.image.FileName);

                        }

                        user.gym_id = (int)Session["gym_id"];
                        user.branch_id = (int)Session["branch_id"];

                        db.users.Add(user);
                        db.SaveChanges();

                        Member member = new Member();
                        member.member_id = user.id;
                        db.members.Add(member);
                        db.SaveChanges();

                        Subscribtion sub = new Subscribtion();

                        sub.cost = memberVM.package_price;
                        sub.member_id = member.member_id;
                        sub.start_date = memberVM.start_date;
                        sub.due_date = memberVM.due_date;
                        sub.note = memberVM.note;
                        sub.offer_id = memberVM.offer_id;
                        sub.package_id = memberVM.package_id;
                        sub.subscribtion_datetime = DateTime.Now;
                        sub.created_at = DateTime.Now;
                        sub.updated_at = DateTime.Now;
                        sub.created_by = Session["id"].ToString().ToInt();
                        db.subscribtions.Add(sub);
                        db.SaveChanges();

                        member.subscribtion_id = sub.id;
                        db.Entry(member).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {

                        User oldUser = db.users.Find(memberVM.id);

                        oldUser.full_name = memberVM.full_name;
                        oldUser.code = memberVM.code;
                        oldUser.email = memberVM.email;
                        oldUser.phone1 = memberVM.phone1;
                        oldUser.phone2 = memberVM.phone2;
                        oldUser.address1 = memberVM.address1;
                        oldUser.address2 = memberVM.address2;
                        oldUser.birthDate = memberVM.birthDate;
                        oldUser.gender = memberVM.gender;
                        oldUser.gym_id = (int)Session["gym_id"];
                        oldUser.branch_id = (int)Session["branch_id"];
                        oldUser.active = memberVM.active;
                        if (memberVM.image != null)
                        {
                            Guid guid = Guid.NewGuid();
                            var InputFileName = Path.GetFileName(memberVM.image.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/images/memberProfile/") + guid.ToString() + "_Profile" + Path.GetExtension(memberVM.image.FileName));
                            memberVM.image.SaveAs(ServerSavePath);
                            oldUser.image = "/images/memberProfile/" + guid.ToString() + "_Profile" + Path.GetExtension(memberVM.image.FileName);
                        }

                        db.Entry(oldUser).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        Member member = db.members.Find(oldUser.id);
                        Subscribtion sub = db.subscribtions.Find(member.subscribtion_id);
                        sub.cost = memberVM.package_price;
                        sub.start_date = memberVM.start_date;
                        sub.due_date = memberVM.due_date;
                        sub.note = memberVM.note;
                        sub.offer_id = memberVM.offer_id;
                        sub.package_id = memberVM.package_id;
                        sub.subscribtion_datetime = DateTime.Now;
                        sub.updated_at = DateTime.Now;
                        sub.created_by = Session["id"].ToString().ToInt();
                        db.Entry(sub).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                    }

                    return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);

                }
            }
            else
            {
                return Json(new { message = "code is already exist" }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { message = "code is not valid" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult deleteMember(int id)
        {
            Member deletedMember = db.members.Find(id);
            db.members.Remove(deletedMember);

            User deletedUser = db.users.Find(id);
            db.users.Remove(deletedUser);

            List<Subscribtion> deletedSubs = db.subscribtions.Where(s => s.member_id == id).ToList();
            foreach (var deletedSub in deletedSubs)
                db.subscribtions.Remove(deletedSub);
            //deletedSubs.RemoveAll(s => s.member_id == id);
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult checkCodeAvailability(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return Json(new { message = "invaild code", is_valid = false }, JsonRequestBehavior.AllowGet);
            }
            var checkAvailabilty = db.users.Any(s => s.code == code);
            if (checkAvailabilty)
            {
                return Json(new { message = "username already taken", is_valid = false }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { message = "valid username", is_valid = true }, JsonRequestBehavior.AllowGet);
        }
    }
}