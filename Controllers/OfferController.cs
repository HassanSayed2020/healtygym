﻿using healthyGym.Auth;
using healthyGym.Helpers;
using healthyGym.Models;
using healthyGym.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace healthyGym.Controllers
{
    [CustomAuthenticationFilter]
    public class OfferController : Controller
    {
        DBContext db = new DBContext();
        // GET: Offer
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var from_date = Request.Form.GetValues("columns[0][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data
                int branchID = (int)Session["branch_id"];
                var offerData = (from offer in db.offers
                                 join u in db.users on offer.created_by equals u.id into usOf
                                 join p in db.packages on offer.package_id equals p.id into pkOf
                                 join b in db.branches on offer.branch_id equals b.id into brOf
                                 from pOf in pkOf.DefaultIfEmpty()
                                 from uOf in usOf.DefaultIfEmpty()
                                 from bOf in brOf.DefaultIfEmpty()
                                 select new OfferViewModel
                                 {
                                     id = offer.id,
                                     name = offer.name,
                                     note = offer.note,
                                     discount_amount = offer.discount_amount,
                                     offer_type = offer.offer_type,
                                     description = offer.description,
                                     is_active = offer.is_active,
                                     due_date = offer.due_date,
                                     branch_id = offer.branch_id,
                                     package_id = offer.package_id,
                                     branch_name = (bOf == null ? String.Empty : bOf.name),
                                     package_name = (pOf == null ? String.Empty : pOf.name),
                                     user_name = (uOf == null ? String.Empty : uOf.user_name),
                                 }).Where(b => b.branch_id == branchID);
                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    offerData = offerData.Where(m => m.name.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.note.ToLower().Contains(searchValue.ToLower()) || m.description.ToLower().Contains(searchValue.ToLower()) || m.package_name.ToLower().Contains(searchValue.ToLower())
                     || m.user_name.ToLower().Contains(searchValue.ToLower()));
                }

                var displayResult = offerData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = offerData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }

            ViewBag.packages = db.packages.Select(s => new { s.id, s.name }).ToList();
            ViewBag.branches = db.branches.Select(s => new { s.id, s.name }).ToList();

            return View();
        }

        [HttpPost]
        public JsonResult saveOffer(OfferViewModel offerVM)
        {
            if ((int)Session["branch_id"] > 0)
            {
              
                    if (offerVM.id == 0)
                    {
                        Offer offer = AutoMapper.Mapper.Map<OfferViewModel, Offer>(offerVM);

                        offer.created_at = DateTime.Now;
                        offer.updated_at = DateTime.Now;
                        offer.created_by = Session["id"].ToString().ToInt();
                        offer.branch_id = (int)Session["branch_id"];

                        db.offers.Add(offer);

                    }
                    else
                    {

                        Offer oldOffer = db.offers.Find(offerVM.id);

                        oldOffer.name = offerVM.name;
                        oldOffer.note = offerVM.note;
                        oldOffer.description = offerVM.description;
                        oldOffer.is_active = offerVM.is_active;
                        oldOffer.branch_id = (int)Session["branch_id"];
                        oldOffer.discount_amount = offerVM.discount_amount;
                        oldOffer.offer_type = offerVM.offer_type;
                        oldOffer.package_id = offerVM.package_id;
                        oldOffer.due_date = offerVM.due_date;

                        db.Entry(oldOffer).State = System.Data.Entity.EntityState.Modified;

                    }
                    db.SaveChanges();

                    return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
                
            }
            return Json(new { message = "branch isnot selected" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult deleteOffer(int id)
        {
            Offer deletedOffer = db.offers.Find(id);

            db.offers.Remove(deletedOffer);
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }
    }
}