﻿using healthyGym.Auth;
using healthyGym.Helpers;
using healthyGym.Models;
using healthyGym.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace healthyGym.Controllers
{
    [CustomAuthenticationFilter]
    public class OrdersShowController : Controller
    {
        DBContext db = new DBContext();
        // GET: OrdersShow
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var from_date = Request.Form.GetValues("columns[0][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data 
                int branchID = (int)Session["branch_id"];
                var orderData = (from order in db.orders
                                 join user in db.users on order.user_id equals user.id
                                 join created_by in db.users on order.created_by equals created_by.id
                                 select new OrderViewModel
                                   {
                                       id = order.id,
                                       price = order.total_price,
                                       quantity = order.total_quantity,
                                       note = order.note,
                                       created_at = order.created_at,
                                       created_at_string = order.created_at.ToString(),
                                       created_by = created_by.full_name,
                                       user_name = user.full_name,
                                       branch_id = order.branch_id
                                 }).Where(b => b.branch_id == branchID);

                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    orderData = orderData.Where(m => m.user_name.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.note.ToLower().Contains(searchValue.ToLower())
                     || m.price.ToString().ToLower().Contains(searchValue.ToLower()) || m.quantity.ToString().ToLower().Contains(searchValue.ToLower()));
                }

                if (!string.IsNullOrEmpty(from_date))
                {
                    if (Convert.ToDateTime(from_date) != DateTime.MinValue)
                    {
                        DateTime from = Convert.ToDateTime(from_date);
                        orderData = orderData.Where(s => s.created_at >= from);
                    }
                }

                if (!string.IsNullOrEmpty(to_date))
                {
                    if (Convert.ToDateTime(to_date) != DateTime.MinValue)
                    {
                        DateTime to = Convert.ToDateTime(to_date);
                        orderData = orderData.Where(s => s.created_at <= to);
                    }
                }

                //total number of rows count
                var displayResult = orderData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = orderData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }

            return View();
        }
    }
}