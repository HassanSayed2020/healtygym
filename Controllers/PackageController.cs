﻿using healthyGym.Auth;
using healthyGym.Helpers;
using healthyGym.Models;
using healthyGym.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace healthyGym.Controllers
{
    [CustomAuthenticationFilter]
    public class PackageController : Controller
    {
        DBContext db = new DBContext();
        // GET: Package
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                //var from_date = Request.Form.GetValues("columns[0][search][value]")[0];
                //var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data    
                int branchID = (int)Session["branch_id"];
                var packageData = (from package in db.packages
                                    join u in db.users on package.created_by equals u.id into usPk
                                    join b in db.branches on package.branch_id equals b.id into brPk
                                    from uPk in usPk.DefaultIfEmpty()
                                    from bPk in brPk.DefaultIfEmpty()
                                   select new PackageViewModel
                                     {
                                         id = package.id,
                                         name = package.name,
                                         note = package.note,
                                         price = package.price,
                                         month = package.month,
                                         coach_price = package.coach_price,
                                         price_after_discount = package.price_after_discount,
                                         discout_percentage = package.discout_percentage,
                                         description = package.description,
                                         is_active = package.is_active,
                                         branch_id = package.branch_id,   
                                         created_by = package.created_by,
                                         no_of_invites = package.no_of_invites,
                                         branch_name = (bPk == null ? String.Empty : bPk.name),
                                         user_name = (uPk == null ? String.Empty : uPk.user_name),
                                     }).Where(b => b.branch_id == branchID);
                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    packageData = packageData.Where(m => m.name.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.note.ToLower().Contains(searchValue.ToLower()) || m.description.ToLower().Contains(searchValue.ToLower())
                     || m.user_name.ToLower().Contains(searchValue.ToLower()) || m.price.ToString().ToLower().Contains(searchValue.ToLower())
                     || m.coach_price.ToString().ToLower().Contains(searchValue.ToLower()) || m.price_after_discount.ToString().ToLower().Contains(searchValue.ToLower()));
                }

                var displayResult = packageData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = packageData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }

            ViewBag.branches = db.branches.Where(b => b.is_active == true).Select(s => new { s.id, s.name }).ToList();

            return View();
        }

        [HttpPost]
        public JsonResult savePackage(PackageViewModel packageVM)
        {
            if ((int) Session["branch_id"] > 0)
            {
                if (packageVM.id == 0)
                {
                    Package package = AutoMapper.Mapper.Map<PackageViewModel, Package>(packageVM);

                    package.created_at = DateTime.Now;
                    package.updated_at = DateTime.Now;
                    package.created_by = Session["id"].ToString().ToInt();
                    package.branch_id = (int)Session["branch_id"];

                    db.packages.Add(package);

                }
                else
                {

                    Package oldpackage = db.packages.Find(packageVM.id);

                    oldpackage.name = packageVM.name;
                    oldpackage.month = packageVM.month;
                    oldpackage.note = packageVM.note;
                    oldpackage.description = packageVM.description;
                    oldpackage.price = packageVM.price;
                    oldpackage.coach_price = packageVM.coach_price;
                    oldpackage.is_active = packageVM.is_active;
                    oldpackage.no_of_invites = packageVM.no_of_invites;
                    oldpackage.branch_id = (int)Session["branch_id"];

                    db.Entry(oldpackage).State = System.Data.Entity.EntityState.Modified;

                }

                db.SaveChanges();

                return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { message = "branch isnot selected" }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult deletePackage(int id)
        {
            Package deletedPackage = db.packages.Find(id);
            db.packages.Remove(deletedPackage);
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

    }
}