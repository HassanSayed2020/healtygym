﻿using healthyGym.Auth;
using healthyGym.Models;
using healthyGym.ViewModel;
using healthyGym.Helpers;
using healthyGym.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using System.Globalization;

namespace healthyGym.Controllers
{
    [CustomAuthenticationFilter]
    public class ProfileController : Controller
    {
        DBContext db = new DBContext();
        // GET: Profile
        public ActionResult Index(string code)
        {
            //CultureInfo culture = new CultureInfo("en-US");
            //DateTime subscribtion_datetime1 = Convert.ToDateTime(subscribtion_datetime, culture);
            //DateTime myDate = DateTime.ParseExact(subscribtion_datetime, "yyyy-MM-dd HH:mm:ss.fff",
            //                           System.Globalization.CultureInfo.InvariantCulture);
            int branchID = (int)Session["branch_id"];
            var profileData = (from subscribtion in db.subscribtions
                           join member in db.members on subscribtion.member_id equals member.member_id
                           join user in db.users on subscribtion.member_id equals user.id
                           join branch in db.branches on user.branch_id equals branch.id
                           join gym in db.gyms on user.gym_id equals gym.id
                           select new ProfileViewModel
                           {
                               user_id = user.id,
                               id = subscribtion.id,
                               code = user.code,
                               full_name = user.full_name,
                               email = user.email,
                               phone1 = user.phone1,
                               phone2 = user.phone2,
                               address1 = user.address1,
                               address2 = user.address2,
                               gender = user.gender,
                               birthDate = user.birthDate,
                               nationality = user.nationality,
                               gym_id = user.gym_id,
                               active = user.active,
                               gym_name = gym.name,
                               branch_name = branch.name,
                               imagePath = user.image,
                               branch_id = user.branch_id,
                               memberSubscribtions = (from subscribtion in db.subscribtions
                                                join package in db.packages on subscribtion.package_id equals package.id
                                                join user in db.users on subscribtion.member_id equals user.id
                                                join creation in db.users on subscribtion.created_by equals creation.id
                                                join offer in db.offers on subscribtion.offer_id equals offer.id into ofSb
                                                join coach in db.users on subscribtion.staff_id equals coach.id into coSb
                                                from oSb in ofSb.DefaultIfEmpty()
                                                from cSb in coSb.DefaultIfEmpty()
                                                select new SubscribtionViewModel
                                                {  
                                                    package_name = package.name,
                                                    package_price = package.price,
                                                    start_date = subscribtion.start_date,
                                                    due_date = subscribtion.due_date,
                                                    package_description = package.description,
                                                    is_expired = DateTime.Now > subscribtion.due_date ? true : false,
                                                    code = user.code,
                                                    offer_name = (oSb == null ? String.Empty : oSb.name),
                                                    offer_description = (oSb == null ? String.Empty : oSb.description),
                                                    coach_name = (cSb == null ? String.Empty : cSb.full_name),
                                                    created_by = creation.user_name,
                                                    subscribtion_datetime = subscribtion.subscribtion_datetime,
                                                    cost = subscribtion.cost,
                                                    note = subscribtion.note,
                                                    is_active = subscribtion.is_active,
                                                    member_id = subscribtion.member_id,
                                                    package_id = subscribtion.package_id,
                                                    offer_id = subscribtion.offer_id,
                                                    staff_id = subscribtion.staff_id,

                                                }).Where(p => p.code == code).OrderByDescending(o => o.due_date).ToList(),
                                                
                           }).Where(s => s.code == code && s.branch_id == branchID).FirstOrDefault();

            TempData["errorMessage"] = null;

            if (profileData == null)
            {
                TempData["errorMessage"] = "Error! Wrong Code or Has no Subscription.";
                return Redirect("/Profile/Member");
            }

            Visit visit = new Visit();
            visit.user_id = profileData.user_id;
            visit.created_at = DateTime.Now;
            db.visits.Add(visit);
            db.SaveChanges();

            return View(profileData);
        }

        public ActionResult Member()
        {
            return View();
        }
    }
}