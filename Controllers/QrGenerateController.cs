﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QRCoder;
using System.Drawing;
using System.IO;
using healthyGym.Models;

namespace healthyGym.Controllers
{
    public class QrGenerateController : Controller
    {
        // GET: QrGenerate
        [HttpGet]
        public ActionResult ActionQrCode()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ActionQrCode(QRModel qr)
        {

            QRCodeGenerator ObjQr = new QRCodeGenerator();

            QRCodeData qrCodeData = ObjQr.CreateQrCode(qr.Message, QRCodeGenerator.ECCLevel.Q);

            Bitmap bitMap = new QRCode(qrCodeData).GetGraphic(20);

            using (MemoryStream ms = new MemoryStream())
            {

                bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                byte[] byteImage = ms.ToArray();

                ViewBag.Url = "data:image/png;base64," + Convert.ToBase64String(byteImage);

            }

            return View();

        }
    }
}