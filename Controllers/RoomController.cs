﻿using healthyGym.Auth;
using healthyGym.Helpers;
using healthyGym.Models;
using healthyGym.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace healthyGym.Controllers
{
    [CustomAuthenticationFilter]
    public class RoomController : Controller
    {
        // GET: Room
        DBContext db = new DBContext();
        // GET: Category
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var from_date = Request.Form.GetValues("columns[0][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data
                int branchID = (int)Session["branch_id"];
                var roomData = (from room in db.rooms
                                select new RoomViewModel
                                {
                                    id = room.id,
                                    Description = room.Description,
                                    branch_id = room.branch_id,
                                    string_created_at = room.created_at.ToString(),
                                    is_active = room.is_active
                                }).Where(g => g.branch_id == branchID);

                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    roomData = roomData.Where(m => m.Description.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()));
                }

                //total number of rows count     

                var displayResult = roomData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = roomData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }
            return View();
        }

        [HttpPost]
        public JsonResult saveRoom(RoomViewModel roomVM)
        {

            if ((int)Session["branch_id"] > 0)
            {
                if (roomVM.id == 0)
                {
                    Room room = AutoMapper.Mapper.Map<RoomViewModel, Room>(roomVM);

                    room.updated_at = DateTime.Now;
                    room.created_by = Session["id"].ToString().ToInt();
                    room.created_at = DateTime.Now;
                    room.branch_id = (int)Session["branch_id"];
                    db.rooms.Add(room);
                }
                else
                {
                    Room oldRoom = db.rooms.Find(roomVM.id);

                    oldRoom.Description = roomVM.Description;
                    oldRoom.is_active = roomVM.is_active;
                    oldRoom.branch_id = (int)Session["branch_id"];
                    oldRoom.updated_at = DateTime.Now;

                    db.Entry(oldRoom).State = System.Data.Entity.EntityState.Modified;
                }
                db.SaveChanges();

                return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { message = "branch is not selected" }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult deleteRoom(int id)
        {
            Room deletedRoom = db.rooms.Find(id);
            db.rooms.Remove(deletedRoom);
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }
    }
}