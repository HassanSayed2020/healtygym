﻿using healthyGym.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace healthyGym.Controllers
{
    public class StatisticsController : Controller
    {
        // GET: Statistics
        public ActionResult SubscribtionChart()
        {
            DateTime dateTime = DateTime.Now;
            var currentYear = dateTime.Year;
            var currentMonth = dateTime.Month;
            string cs = ConfigurationManager.ConnectionStrings["DBContextADO"].ConnectionString;

            SqlConnection sql = new SqlConnection(cs);
            sql.Open();

            SqlCommand comm = new SqlCommand("select YEAR(subscribtion_datetime) as years, MONTH(subscribtion_datetime) as months, count(*) as subscribtion_count from Subscribtions group by YEAR(subscribtion_datetime) , MONTH(subscribtion_datetime)", sql);
            SqlDataReader reader = comm.ExecuteReader();
            List<int> data = new List<int>();
            List<string> xAxis = new List<string>();

            while (reader.Read())
            {
                data.Add(reader["subscribtion_count"].ToString().ToInt());
                xAxis.Add(reader["months"].ToString() + '/' + reader["years"].ToString());
            }
            var subscribtionSum =  data.Sum();
            
            reader.Close();
            
            sql.Close();
            var subscribtionAverage = 0;
            if (data.Count() > 0)
            {
                 subscribtionAverage = (int)subscribtionSum / data.Count();
            }
            
            return Json(new { data = data, xAxis = xAxis, subscribtionSum = subscribtionSum, subscribtionAverage = subscribtionAverage, message = "done"},JsonRequestBehavior.AllowGet);
        }

        public ActionResult VisitorChart()
        {
            DateTime dateTime = DateTime.Now;
            var currentYear = dateTime.Year;
            var currentMonth = dateTime.Month;
            string cs = ConfigurationManager.ConnectionStrings["DBContextADO"].ConnectionString;

            SqlConnection sql = new SqlConnection(cs);
            sql.Open();

            SqlCommand comm = new SqlCommand("select YEAR(created_at) as years, MONTH(created_at) as months, count(*) as visitor_count from Visits group by YEAR(created_at) , MONTH(created_at)", sql);
            SqlDataReader reader = comm.ExecuteReader();
            List<int> data = new List<int>();
            List<string> xAxis = new List<string>();

            while (reader.Read())
            {
                data.Add(reader["visitor_count"].ToString().ToInt());
                xAxis.Add(reader["months"].ToString() + '/' + reader["years"].ToString());
            }
            var visitorSum = data.Sum();

            reader.Close();

            sql.Close();

            var visitorAverage = 0;
            if (data.Count() > 0)
            {
                visitorAverage = (int)visitorSum / data.Count();
            }

            return Json(new { data = data, xAxis = xAxis, visitorSum = visitorSum, visitorAverage = visitorAverage, message = "done" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PaymentChart()
        {
            DateTime dateTime = DateTime.Now;
            var currentYear = dateTime.Year;
            var currentMonth = dateTime.Month;
            string cs = ConfigurationManager.ConnectionStrings["DBContextADO"].ConnectionString;

            SqlConnection sql = new SqlConnection(cs);
            sql.Open();

            SqlCommand comm = new SqlCommand("select YEAR(created_at) as years, MONTH(created_at) as months, sum(cost) as total_amount from Subscribtions group by YEAR(created_at) , MONTH(created_at)", sql);
            SqlDataReader reader = comm.ExecuteReader();
            List<int> data = new List<int>();
            List<string> xAxis = new List<string>();

            while (reader.Read())
            {
                data.Add(reader["total_amount"].ToString().ToInt());
                xAxis.Add(reader["months"].ToString() + '/' + reader["years"].ToString());
            }
            var paymentSum = data.Sum();

            reader.Close();

            sql.Close();

            var paymentAverage = 0;
            if (data.Count() > 0)
            {
                paymentAverage = (int)paymentSum / data.Count();
            }

            return Json(new { data = data, xAxis = xAxis, paymentSum = paymentSum, paymentAverage = paymentAverage, message = "done" }, JsonRequestBehavior.AllowGet);
        }

    }
}