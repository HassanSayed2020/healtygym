﻿using healthyGym.Auth;
using healthyGym.Models;
using healthyGym.ViewModel;
using healthyGym.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using healthyGym.Enums;

namespace healthyGym.Controllers
{
    [CustomAuthenticationFilter]
    public class SubscribtionController : Controller
    {
        DBContext db = new DBContext();
        // GET: Subscribtion
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var subFilter = Request.Form.GetValues("columns[0][search][value]")[0];
                var from_date = Request.Form.GetValues("columns[1][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[2][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data    
                int branchID = (int)Session["branch_id"];
                var subData = (from subscribtion in db.subscribtions
                                  join member in db.members on subscribtion.member_id equals member.member_id
                                  join user in db.users on subscribtion.member_id equals user.id
                                  join branch in db.branches on user.branch_id equals branch.id
                                  join gym in db.gyms on user.gym_id equals gym.id
                                  join creation in db.users on subscribtion.created_by equals creation.id
                                  join package in db.packages on subscribtion.package_id equals package.id
                                  join offer in db.offers on subscribtion.offer_id equals offer.id into ofSb
                                  join coach in db.users on subscribtion.staff_id equals coach.id into coSb
                                  from oSb in ofSb.DefaultIfEmpty()
                                  from cSb in coSb.DefaultIfEmpty()
                                  select new SubscribtionViewModel
                                  {
                                        id = user.id,
                                        code = user.code,
                                        full_name = user.full_name,
                                        email = user.email,
                                        phone1 = user.phone1,
                                        phone2 = user.phone2,
                                        address1 = user.address1,
                                        address2 = user.address2,
                                        gender = user.gender,
                                        birthDate = user.birthDate,
                                        nationality = user.nationality,
                                        gym_id = user.gym_id,
                                        active = user.active,
                                        branch_id = user.branch_id,
                                        gym_name = gym.name,
                                        branch_name = branch.name,
                                        created_by = creation.user_name,
                                        subscribtion_datetime = subscribtion.subscribtion_datetime,
                                        cost = subscribtion.cost,
                                        note = subscribtion.note,
                                        start_date = subscribtion.start_date,
                                        due_date = subscribtion.due_date,
                                        start_date_string = subscribtion.start_date.ToString(),
                                        due_date_string = subscribtion.due_date.ToString(),
                                        is_active = subscribtion.is_active,
                                        member_id = subscribtion.member_id,
                                        package_id = subscribtion.package_id,
                                        offer_id = subscribtion.offer_id,
                                        staff_id = subscribtion.staff_id,
                                        subscribtion_id = subscribtion.id,
                                        created_at = subscribtion.created_at,
                                        package_name = package.name,
                                        package_month = package.month,
                                        package_price = package.price,
                                        offer_name = (oSb == null ? String.Empty : oSb.name),
                                        offer_amount = (oSb == null ? 0: oSb.discount_amount),
                                        offer_type = (oSb == null ? 0 : oSb.offer_type),
                                        coach_name = (cSb == null ? String.Empty : cSb.full_name)

                                  }).Where(b => b.branch_id == branchID);
                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    subData = subData.Where(m => m.full_name.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.email.ToLower().Contains(searchValue.ToLower()) || m.phone1.ToLower().Contains(searchValue.ToLower()) || m.package_name.ToLower().Contains(searchValue.ToLower())
                     || m.user_name.ToLower().Contains(searchValue.ToLower()));
                }

                if (!string.IsNullOrEmpty(subFilter))
                {
                    if (Convert.ToInt32(subFilter) == 2)
                    {
                        subData = subData.Where(m => m.due_date < DateTime.Now);
                    }
                    else
                    {
                        subData = subData.Where(m => m.due_date >= DateTime.Now);
                    }
                }
                else
                {
                    subData = subData.Where(m => m.due_date >= DateTime.Now);
                }

                if (!string.IsNullOrEmpty(from_date))
                {
                    if (Convert.ToDateTime(from_date) != DateTime.MinValue)
                    {
                        DateTime from = Convert.ToDateTime(from_date);
                        subData = subData.Where(s => s.created_at >= from);
                    }
                }

                if (!string.IsNullOrEmpty(to_date))
                {
                    if (Convert.ToDateTime(to_date) != DateTime.MinValue)
                    {
                        DateTime to = Convert.ToDateTime(to_date);
                        subData = subData.Where(s => s.created_at <= to);
                    }
                }

                var displayResult = subData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = subData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }

            ViewBag.packages = db.packages.Select(s => new { s.id, s.name }).ToList();
            ViewBag.offers = db.offers.Select(s => new { s.id, s.name }).ToList();
            ViewBag.branches = db.branches.Select(s => new { s.id, s.name }).ToList();
            ViewBag.gyms = db.gyms.Select(s => new { s.id, s.name }).ToList();
            ViewBag.coaches = (from staff in db.staffs
                               join user in db.users on staff.staff_id equals user.id
                               join staffRole in db.staffRoles on staff.staff_role_id equals staffRole.id
                               select new CoachViewModel
                               {
                                   id = staff.staff_id,
                                   full_name = user.full_name,
                                   role = staffRole.role
                               }).Where(s => s.role == "Coach").ToList();
            ViewBag.members = (from member in db.members
                               join user in db.users on member.member_id equals user.id
                               select new MemberViewModel
                               {
                                   id = member.member_id,
                                   full_name = user.full_name,
                                   type = user.type
                               }).Where(s => s.type == (int)UserTypes.Staff).ToList();

            return View();
        }

        [HttpPost]
        public JsonResult saveSubscribtion(SubscribtionViewModel subVM)
        {

            if (subVM.id == 0)
            {

                Subscribtion sub = AutoMapper.Mapper.Map<SubscribtionViewModel, Subscribtion>(subVM);
               
                sub.cost = subVM.package_price;
                sub.subscribtion_datetime = DateTime.Now;
                sub.created_at = DateTime.Now;
                sub.updated_at = DateTime.Now;
                sub.created_by = Session["id"].ToString().ToInt();
                db.subscribtions.Add(sub);
                db.SaveChanges();

            }
            else
            {

                Subscribtion oldSub = db.subscribtions.Where(s => s.id == subVM.id).FirstOrDefault();
                
                if(oldSub.package_id != subVM.package_id)
                {
                    db.subscribtions.Remove(oldSub);
                    db.SaveChanges();
                    
                    oldSub.cost = subVM.package_price;
                    oldSub.note = subVM.note;
                    oldSub.due_date = subVM.due_date;
                    oldSub.start_date = subVM.start_date;
                    oldSub.staff_id = subVM.staff_id;
                    oldSub.package_id = subVM.package_id;
                    oldSub.offer_id = subVM.offer_id;
                    oldSub.is_active = subVM.is_active;
                    db.subscribtions.Add(oldSub);

                }
                else
                {

                    oldSub.cost = subVM.package_price;
                    oldSub.note = subVM.note;
                    oldSub.due_date = subVM.due_date;
                    oldSub.start_date = subVM.start_date;
                    oldSub.staff_id = subVM.staff_id == null ? 0 : subVM.staff_id;
                    oldSub.offer_id = subVM.offer_id == null ? 0 : subVM.offer_id;
                    oldSub.is_active = subVM.is_active;

                    db.Entry(oldSub).State = System.Data.Entity.EntityState.Modified;

                }

                db.SaveChanges();

            }



            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult deleteSubscribtion(int id)
        {
            Subscribtion deletedSubscribtion = db.subscribtions.Find(id);
            db.subscribtions.Remove(deletedSubscribtion);
            //List<Subscribtion> deletedSubs = db.subscribtions.Where(s => s.member_id == id).ToList();
            //foreach (var deletedSub in deletedSubs)
            //    db.subscribtions.Remove(deletedSub);
            //deletedSubs.RemoveAll(s => s.member_id == id);
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getCost(int package_id, int offer_id )
        {
            double cost = 0;
            if (package_id > 0) {
                Package package = db.packages.Find(package_id);
                cost = package.price;
                if (offer_id > 0)
                {
                    Offer offer = db.offers.Find(offer_id);
                    if (offer.offer_type == 1)
                        cost = (double)(cost * offer.discount_amount / 100);
                    else
                        cost = (double)(cost - offer.discount_amount);
                }
            
            }
          
            return Json(new { data = cost }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult calculateDueDate(int package_id, DateTime start_date)
        {
            DateTime dueDate = DateTime.Now;
            if (package_id > 0)
            {
                Package package = db.packages.Find(package_id);
                dueDate = start_date.AddMonths(package.month);

            }

            return Json(new { data = dueDate }, JsonRequestBehavior.AllowGet);
        }

    }
}