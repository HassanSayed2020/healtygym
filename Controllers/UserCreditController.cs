﻿using healthyGym.Auth;
using healthyGym.Models;
using healthyGym.ViewModel;
using healthyGym.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using healthyGym.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace healthyGym.Controllers
{
    [CustomAuthenticationFilter]
    public class UserCreditController : Controller
    {
        DBContext db = new DBContext();
        // GET: UserCredit
        public ActionResult Index()
        {

            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var from_date = Request.Form.GetValues("columns[0][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                // Getting all data
                int branchID = (int)Session["branch_id"];
                var creditData = (from credit in db.userCredits
                                  join user in db.users on credit.user_id equals user.id
                                  join createdby in db.users on credit.created_by equals createdby.id
                                  select new UserCreditViewModel
                                  {
                                      id = credit.id,
                                      amount = credit.amount,
                                      credit_type = credit.credit_type,
                                      note = credit.note,
                                      created_at = credit.created_at.ToString(),
                                      updated_at = credit.updated_at.ToString(),
                                      created_by = createdby.full_name,
                                      full_name = user.full_name,
                                      code = user.code,
                                      user_id = user.id,
                                      branch_id = user.branch_id
                                  }).Where(b => b.branch_id == branchID);

                //Search    
                if (!string.IsNullOrEmpty(searchValue))
                {
                    creditData = creditData.Where(m => m.full_name.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.note.ToLower().Contains(searchValue.ToLower()) || m.amount.ToString().ToLower().Contains(searchValue.ToLower())
                     || m.created_by.ToLower().Contains(searchValue.ToLower()));
                }

                //total number of rows count     

                var displayResult = creditData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = creditData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }
            ViewBag.gyms = db.gyms.Select(s => new { s.id, s.name }).ToList();
            return View();
        }

        [HttpPost]
        public JsonResult saveUserCredit(UserCreditViewModel userCreditVM)
        {
         
            //var check = (JObject)JsonConvert.DeserializeObject(checkCode(userCreditVM.code).ToString());
            if ((int)Session["branch_id"] > 0)
            {
                if (userCreditVM.id == 0)
                {
                    UserCredit userCredit = AutoMapper.Mapper.Map<UserCreditViewModel, UserCredit>(userCreditVM);

                    userCredit.updated_at = DateTime.Now;
                    userCredit.created_by = Session["id"].ToString().ToInt();
                    userCredit.created_at = DateTime.Now;

                    var user = db.users.Where(u => u.code == userCreditVM.code).FirstOrDefault();

                    userCredit.user_id = user.id;

                    db.userCredits.Add(userCredit);
                }
                else
                {
                    UserCredit oldUserCredit = db.userCredits.Find(userCreditVM.id);

                    oldUserCredit.amount = userCreditVM.amount;
                    oldUserCredit.credit_type = userCreditVM.credit_type;
                    oldUserCredit.note = userCreditVM.note;
                    oldUserCredit.updated_at = DateTime.Now;

                    var user = db.users.Where(u => u.code == userCreditVM.code).FirstOrDefault();

                    oldUserCredit.user_id = user.id;

                    db.Entry(oldUserCredit).State = System.Data.Entity.EntityState.Modified;
                }
                db.SaveChanges();

                return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { message = "branch is not selected" }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult deleteUserCredit(int id)
        {
            UserCredit deleteUserCredit = db.userCredits.Find(id);
            db.userCredits.Remove(deleteUserCredit);
            db.SaveChanges();

            return Json(new { message = "done" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult checkCode(string code)
        {
            var user = (from userCheck in db.users
                        join member in db.members on userCheck.id equals member.member_id
                         select new UserCreditViewModel
                         {
                             code = userCheck.code,
                             full_name = userCheck.full_name
                         }).Where(u => u.code == code).FirstOrDefault();

            if (user != null)
            {
                return Json(new { message = "valid code", is_valid = true, full_name = user.full_name }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { message = "wrong code", is_valid = false }, JsonRequestBehavior.AllowGet);
        }

    }
}