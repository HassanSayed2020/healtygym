﻿using healthyGym.Auth;
using healthyGym.Models;
using healthyGym.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace healthyGym.Controllers
{
    [CustomAuthenticationFilter]
    public class VisitController : Controller
    {
        DBContext db = new DBContext();
        // GET: Visit
        public ActionResult Index()
        {

            if (Request.IsAjaxRequest())
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                var from_date = Request.Form.GetValues("columns[0][search][value]")[0];
                var to_date = Request.Form.GetValues("columns[1][search][value]")[0];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                int branchID = (int)Session["branch_id"];
                // Getting all data    
                var visitData = (from v in db.visits
                                    join u in db.users on v.user_id equals u.id
                                    select new VisitViewModel {
                                        id = v.id,
                                        full_name = u.full_name,
                                        created_at = v.created_at.ToString(),
                                        branch_id = u.branch_id,
                                        email = u.email,
                                        address1 = u.address1,
                                        address2 = u.address2,
                                        phone1 = u.phone1,
                                        phone2 = u.phone2,
                                        code = u.code
                                    }).Where(v => v.branch_id == branchID);

                //Search
                if (!string.IsNullOrEmpty(searchValue))
                {
                    visitData = visitData.Where(m => m.full_name.ToLower().Contains(searchValue.ToLower()) || m.id.ToString().ToLower().Contains(searchValue.ToLower()) ||
                     m.phone1.ToLower().Contains(searchValue.ToLower()) || m.phone2.ToLower().Contains(searchValue.ToLower()) || m.email.ToLower().Contains(searchValue.ToLower())
                     || m.address1.ToLower().Contains(searchValue.ToLower()));
                }

                //if (!string.IsNullOrEmpty(from_date))
                //{
                //    //CultureInfo provider = CultureInfo.InvariantCulture;
                //    //DateTime dateTime10 = DateTime.ParseExact(from_date, "yyyy/mm/dd", provider);
                //    gymData = gymData.Where(m => Convert.ToDateTime(m.created_at) >= Convert.ToDateTime(from_date));
                //}

                //total number of rows count     

                var displayResult = visitData.OrderByDescending(u => u.id).Skip(skip)
                     .Take(pageSize).ToList();
                var totalRecords = visitData.Count();

                return Json(new
                {
                    draw = draw,
                    recordsTotal = totalRecords,
                    recordsFiltered = totalRecords,
                    data = displayResult

                }, JsonRequestBehavior.AllowGet);

            }

            return View();
        }
    }
}