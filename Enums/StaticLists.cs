﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace healthyGym.Enums
{
    public enum UserTypes
    {
        SuperAdmin = 1,
        Owner = 2,
        Admin = 3,
        Staff = 4,
        Member = 5,
    }

    public enum Gender
    {
        Male = 1,
        Female = 2,
    }
}