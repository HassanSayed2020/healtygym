﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using healthyGym.ViewModel;
namespace healthyGym.Helpers
{
    public static class Helpers
    {
        public static int ToInt(this string str)
        {
            return Convert.ToInt32(str);
        }
        public static int ToDate(this string str)
        {
            return Convert.ToInt32(str);
        }

        public static double getProductQuantity(int id)
        {
            double quantity = 0;
            List<OrderViewModel> orderDetails = System.Web.HttpContext.Current.Session["order"] as List<OrderViewModel>;
            foreach (OrderViewModel ovm in orderDetails)
            {
                if (ovm.id == id)
                {
                    quantity = ovm.quantity;
                    break;
                }

            }
            return quantity;
        }
    }
}