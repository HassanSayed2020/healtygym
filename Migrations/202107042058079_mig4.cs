﻿namespace healthyGym.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "gym_id", "dbo.Gyms");
            DropForeignKey("dbo.Branches", "gym_id", "dbo.Gyms");
            DropForeignKey("dbo.Subscribtions", "member_id", "dbo.Members");
            DropForeignKey("dbo.Subscribtions", "package_id", "dbo.Packeges");
            DropIndex("dbo.Branches", new[] { "gym_id" });
            DropIndex("dbo.Users", new[] { "gym_id" });
            DropIndex("dbo.Subscribtions", new[] { "member_id" });
            DropIndex("dbo.Subscribtions", new[] { "package_id" });
            DropPrimaryKey("dbo.Subscribtions");
            CreateTable(
                "dbo.PackegeMembers",
                c => new
                    {
                        Packege_id = c.Int(nullable: false),
                        Member_member_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Packege_id, t.Member_member_id })
                .ForeignKey("dbo.Packeges", t => t.Packege_id, cascadeDelete: true)
                .ForeignKey("dbo.Members", t => t.Member_member_id, cascadeDelete: true)
                .Index(t => t.Packege_id)
                .Index(t => t.Member_member_id);
            
            AddColumn("dbo.Subscribtions", "subscribtion_datetime", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Subscribtions", "packege_id", c => c.Int());
            AlterColumn("dbo.Subscribtions", "member_id", c => c.Int(nullable: false));
            AlterColumn("dbo.Subscribtions", "package_id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Subscribtions", new[] { "member_id", "package_id", "subscribtion_datetime" });
            CreateIndex("dbo.Subscribtions", "member_id");
            CreateIndex("dbo.Subscribtions", "packege_id");
            AddForeignKey("dbo.Subscribtions", "member_id", "dbo.Members", "member_id", cascadeDelete: true);
            AddForeignKey("dbo.Subscribtions", "packege_id", "dbo.Packeges", "id");
            DropColumn("dbo.Branches", "gym_id");
            DropColumn("dbo.Users", "gym_id");
            DropColumn("dbo.Subscribtions", "id");
            DropTable("dbo.Gyms");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Gyms",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        code = c.String(),
                        name = c.String(),
                        description = c.String(),
                        is_active = c.Boolean(),
                        created_by = c.Int(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.Subscribtions", "id", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.Users", "gym_id", c => c.Int());
            AddColumn("dbo.Branches", "gym_id", c => c.Int());
            DropForeignKey("dbo.Subscribtions", "packege_id", "dbo.Packeges");
            DropForeignKey("dbo.Subscribtions", "member_id", "dbo.Members");
            DropForeignKey("dbo.PackegeMembers", "Member_member_id", "dbo.Members");
            DropForeignKey("dbo.PackegeMembers", "Packege_id", "dbo.Packeges");
            DropIndex("dbo.PackegeMembers", new[] { "Member_member_id" });
            DropIndex("dbo.PackegeMembers", new[] { "Packege_id" });
            DropIndex("dbo.Subscribtions", new[] { "packege_id" });
            DropIndex("dbo.Subscribtions", new[] { "member_id" });
            DropPrimaryKey("dbo.Subscribtions");
            AlterColumn("dbo.Subscribtions", "package_id", c => c.Int());
            AlterColumn("dbo.Subscribtions", "member_id", c => c.Int());
            DropColumn("dbo.Subscribtions", "packege_id");
            DropColumn("dbo.Subscribtions", "subscribtion_datetime");
            DropTable("dbo.PackegeMembers");
            AddPrimaryKey("dbo.Subscribtions", "id");
            CreateIndex("dbo.Subscribtions", "package_id");
            CreateIndex("dbo.Subscribtions", "member_id");
            CreateIndex("dbo.Users", "gym_id");
            CreateIndex("dbo.Branches", "gym_id");
            AddForeignKey("dbo.Subscribtions", "package_id", "dbo.Packeges", "id");
            AddForeignKey("dbo.Subscribtions", "member_id", "dbo.Members", "member_id");
            AddForeignKey("dbo.Branches", "gym_id", "dbo.Gyms", "id");
            AddForeignKey("dbo.Users", "gym_id", "dbo.Gyms", "id");
        }
    }
}
