﻿namespace healthyGym.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Gyms",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        code = c.String(),
                        name = c.String(),
                        description = c.String(),
                        is_active = c.Boolean(),
                        created_by = c.Int(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.Branches", "gym_id", c => c.Int());
            AddColumn("dbo.Users", "gym_id", c => c.Int());
            CreateIndex("dbo.Branches", "gym_id");
            CreateIndex("dbo.Users", "gym_id");
            AddForeignKey("dbo.Users", "gym_id", "dbo.Gyms", "id");
            AddForeignKey("dbo.Branches", "gym_id", "dbo.Gyms", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Branches", "gym_id", "dbo.Gyms");
            DropForeignKey("dbo.Users", "gym_id", "dbo.Gyms");
            DropIndex("dbo.Users", new[] { "gym_id" });
            DropIndex("dbo.Branches", new[] { "gym_id" });
            DropColumn("dbo.Users", "gym_id");
            DropColumn("dbo.Branches", "gym_id");
            DropTable("dbo.Gyms");
        }
    }
}
