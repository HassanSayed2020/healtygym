﻿// <auto-generated />
namespace healthyGym.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class addCoach : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addCoach));
        
        string IMigrationMetadata.Id
        {
            get { return "202107082211023_addCoach"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
