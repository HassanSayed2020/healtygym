﻿namespace healthyGym.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatesubscribtionkey : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Subscribtions");
            AddColumn("dbo.Subscribtions", "id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Subscribtions", "id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Subscribtions");
            DropColumn("dbo.Subscribtions", "id");
            AddPrimaryKey("dbo.Subscribtions", new[] { "member_id", "package_id", "subscribtion_datetime" });
        }
    }
}
