﻿namespace healthyGym.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class roomsAndSchedule : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Rooms",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        is_active = c.Boolean(),
                        created_by = c.Int(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Schedules",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        timeZone = c.String(),
                        allDay = c.Boolean(nullable: false),
                        title = c.String(),
                        start = c.DateTime(nullable: false),
                        end = c.DateTime(nullable: false),
                        className = c.String(),
                        description = c.String(),
                        user_id = c.Int(nullable: false),
                        room_id = c.Int(nullable: false),
                        package_id = c.Int(nullable: false),
                        is_active = c.Boolean(),
                        created_by = c.Int(),
                        created_at = c.DateTime(precision: 7, storeType: "datetime2"),
                        updated_at = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Schedules");
            DropTable("dbo.Rooms");
        }
    }
}
