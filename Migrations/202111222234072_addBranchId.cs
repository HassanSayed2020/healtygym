﻿namespace healthyGym.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addBranchId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rooms", "branch_id", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Rooms", "branch_id");
        }
    }
}
