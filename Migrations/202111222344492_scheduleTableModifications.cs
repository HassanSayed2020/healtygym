﻿namespace healthyGym.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class scheduleTableModifications : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Schedules", "branch_id", c => c.Int());
            AlterColumn("dbo.Schedules", "user_id", c => c.Int());
            AlterColumn("dbo.Schedules", "room_id", c => c.Int());
            AlterColumn("dbo.Schedules", "package_id", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Schedules", "package_id", c => c.Int(nullable: false));
            AlterColumn("dbo.Schedules", "room_id", c => c.Int(nullable: false));
            AlterColumn("dbo.Schedules", "user_id", c => c.Int(nullable: false));
            DropColumn("dbo.Schedules", "branch_id");
        }
    }
}
