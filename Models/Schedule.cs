﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace healthyGym.Models
{
    public class Schedule
    {
        [Key]
        public int id { get; set; }
        public string timeZone { get; set; }
        public bool allDay { get; set; }
        public string title { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public string className { get; set; }
        public string description { get; set; }
        public int? user_id { get; set; }
        public int? room_id { get; set; }
        public int? package_id { get; set; }
        public int? branch_id { get; set; }
        public bool? is_active { get; set; }
        public int? created_by { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? created_at { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? updated_at { get; set; }


    }
}