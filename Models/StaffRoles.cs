﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace healthyGym.Models
{
    public class StaffRoles
    {
        [Key]
        public int id { get; set; }
        public string role { get; set; }
        public string description { get; set; }
        public string note { get; set; }
        public int? created_by { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? created_at { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? updated_at { get; set; }
        public virtual ICollection<Staff> staffs { get; set; }

    }
}