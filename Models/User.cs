﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace healthyGym.Models
{
    public class User
    {
        [Key]
        public int id { get; set; }
        public string code { get; set; }
        public string user_name { get; set; }
        public string full_name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string phone1 { get; set; }
        public string phone2 { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public int gender { get; set; }
        public string nationality { get; set; }
        public string gym_code { get; set; }
        [Column(TypeName = "date")]
        public DateTime? birthDate { get; set; }
        public string image { get; set; }
        public int type { get; set; }
        public int? active { get; set; }
        public int? created_by { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? created_at { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? updated_at { get; set; }
        [ForeignKey("Gym")]
        public int? gym_id { get; set; }
        public Gym Gym { get; set; }

        [ForeignKey("Branch")]
        public int? branch_id { get; set; }
        public Branch Branch { get; set; }
        public virtual Member Member { get; set; }
        public virtual Staff Staff { get; set; }
        public virtual ICollection<Visit> Visits { get; set; }
        public virtual ICollection<UserCredit> UserCredits { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}