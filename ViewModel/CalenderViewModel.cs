﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace healthyGym.ViewModel
{
    public class CalenderViewModel
    {
        public int id { get; set; }
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public string className { get; set; }
        public string description { get; set; }
    }
}