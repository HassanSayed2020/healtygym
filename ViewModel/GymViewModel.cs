﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace healthyGym.ViewModel
{
    public class GymViewModel
    {

        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool? is_active   { get; set; }

        public string created_by { get; set; }
    
        public string created_at { get; set; }
     
        public string updated_at { get; set; }
        public string user_name { get; set; }
        public string password { get; set; }
        public string address1 { get; set; }
        public string phone1 { get; set; }
        public string email { get; set; }
        public string imagePath { get; set; }
        public int type { get; set; }
        public string full_name { get; set; }
        public HttpPostedFileBase image { get; set; }
        public List<BranchViewModel> branches { get; set; }
    }
}