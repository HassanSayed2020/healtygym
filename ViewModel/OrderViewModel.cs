﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace healthyGym.ViewModel
{
    public class OrderViewModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public double price { get; set; }
        public double quantity { get; set; }
        public string note { get; set; }
        public int? branch_id { get; set; }
        public HttpPostedFileBase image { get; set; }
        public string productImage { get; set; }
        public string user_name { get; set; }
        public DateTime? created_at { get; set; }
        public string created_at_string { get; set; }
        public string created_by { get; set; }
    }
}