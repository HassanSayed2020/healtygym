﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace healthyGym.ViewModel
{
    public class PackageViewModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public double price { get; set; }
        public double coach_price { get; set; }
        public double price_after_discount { get; set; }
        public double discout_percentage { get; set; }
        public int no_of_invites { get; set; }
        public string note { get; set; }
        public int month { get; set; }
        public bool? is_active { get; set; }
        public int? branch_id { get; set; }
        public string branch_name { get; set; }
        public int? created_by { get; set; }
        public string user_name { get; set; }
        public DateTime? created_at { get; set; }
  
        public DateTime? updated_at { get; set; }
        public string code { get; set; }



    }
}