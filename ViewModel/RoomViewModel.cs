﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace healthyGym.ViewModel
{
    public class RoomViewModel
    {
        public int id { get; set; }
        public string Description { get; set; }
        public int? branch_id { get; set; }
        public bool? is_active { get; set; }
        public int? created_by { get; set; }
        public string string_created_at { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }

    }
}