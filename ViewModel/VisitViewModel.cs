﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace healthyGym.ViewModel
{
    public class VisitViewModel
    {
        public int id { get; set; }
        public string created_at { get; set; }
        public int? user_id { get; set; }
        public string full_name { get; set; }
        public int? branch_id { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string phone1 { get; set; }
        public string phone2 { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public int gender { get; set; }
        public string code { get; set; }
    }
}